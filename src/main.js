// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import store from './store'
import Axios from "axios"


Vue.prototype.$http = Axios;
Vue.use(BootstrapVue);

Axios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  console.log(error.response.data);
  if (error.response.data.status === 401) {
    store.dispatch('logout').then(() => router.push("/login"))
      .catch(err => console.log(err));
  }
  return Promise.reject(error)
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})
