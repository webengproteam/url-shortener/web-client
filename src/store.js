import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import { API_BASE, DATA_API_BASE } from "./config";

axios.defaults.withCredentials = true;

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    status: "",
    public_id: "",
    name: "",
    token: "",
    uris: [],
    hooks: []
  },
  mutations: {
    auth_request(state) {
      state.status = "loading";
    },
    auth_success(state, data) {
      state.status = "success";
      state.name = data.name;
      state.public_id = data.public_id;
      state.token = data.token;

      localStorage.setItem("public_id", data.public_id);
      localStorage.setItem("name", data.name);
      localStorage.setItem("token", data.token);
      console.log("Auth sucessful", state);
    },
    auth_error(state) {
      state.status = "error";
    },
    logout(state) {
      localStorage.removeItem("public_id");
      localStorage.removeItem("token");
      state.public_id = "";
      state.token = "";
      state.name = "";
      state.status = "";
    },
    retrieve_request(state) {
      state.status = "loading";
    },
    retrieve_success(state, data) {
      state.status = "success";
      state.uris = data;
    },
    retrieveHook_success(state, data) {
      state.status = "success";
      state.hooks = data;
    },
    retrieveData_success(state) {
      state.status = "success";
    },
    retrieve_error(state) {
      state.status = "error";
    },
    update_request(state) {
      state.status = "loading";
    },
    update_success(state, data) {
      state.status = "success";
      state.currentUser = data;
    },
    update_error(state) {
      state.status = "error";
    },
    upload_request(state) {
      state.status = "loading";
    },
    upload_success(state) {
      state.status = "success";
    },
    upload_error(state) {
      state.status = "error";
    },
    delete_request(state) {
      state.status = "loading";
    },
    delete_success(state) {
      state.status = "success";
    },
    delete_error(state) {
      state.status = "error";
    },
    activate_request(state) {
      state.status = "loading";
    },
    activate_success(state) {
      state.status = "success";
    },
    activate_error(state) {
      state.status = "error";
    },
    deactivate_request(state) {
      state.status = "loading";
    },
    deactivate_success(state) {
      state.status = "success";
    },
    deactivate_error(state) {
      state.status = "error";
    },
    resetState (state) {
      state.status = "";
      state.public_id = "no_user";
      state.token = "";
    }
  },
  actions: {
    login({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit("auth_request");
        axios({
          url: `${API_BASE}users/login`,
          data: user,
          method: "POST"
        })
          .then(resp => {
            console.log(resp);

            const public_id = resp.data.user.userId;

            localStorage.setItem("public_id", public_id);
            localStorage.setItem("name", resp.data.user.email);
            localStorage.setItem("token", resp.data.token);
            console.log(resp.data.userId);
            commit("auth_success", {
              name: resp.data.user.email,
              public_id: resp.data.user.userId,
              token: resp.data.token,
            });

            axios.defaults.headers.common["Authorization"] = `Bearer ${resp.data.token}`;

            resolve(resp);
          })
          .catch(err => {
            commit("auth_error");
            localStorage.removeItem("public_id");

            reject(err);
          });
      });
    },
    register({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit("auth_request");
        axios({
          url: `${API_BASE}users`,
          data: user,
          method: "POST"
        })
          .then(resp => {
            console.log(resp);
            const public_id = resp.data.public_id;

            localStorage.setItem("public_id", public_id);

            // Add the following line:
            resolve(resp);
          })
          .catch(err => {
            commit("auth_error", err);
            localStorage.removeItem("public_id");

            reject(err);
          });
      });
    },
    logout({ commit }) {
      localStorage.removeItem("public_id");
      localStorage.removeItem("token");
      commit("logout");

    },
    retrieveProfile({ commit }) {
      return new Promise( (resolve, reject) => {
        const user_id = localStorage.getItem("public_id");
        commit("retrieve_request");
        axios({
          url: `${API_BASE}users/${user_id}/uris`,
          data: localStorage.getItem("public_id"),
          method: "GET"
        })
          .then( resp => {
            console.log(resp);
            localStorage.setItem("uris",resp.data);
            commit("retrieve_success", resp.data);
            resolve(resp);
          })
          .catch( err => {
            commit("retrieve_error");
            localStorage.removeItem("uris");
            reject(err);
          })
      });
    },
    updateProfile({ commit }, user) {
      return new Promise( (resolve, reject) =>  {
        const user_id = localStorage.getItem("public_id");
        commit("update_request");
        axios({
          url: `${API_BASE}user/${user_id}/edit`,
          data: user,
          method: "POST"
        })
          .then( resp => {
            console.log(resp);
            localStorage.setItem("currentUser", user);
            commit("update_success", user);
            resolve(resp);
          })
          .catch( err => {
            commit("update_error");
            reject(err);
          })
      });
    },
    newUri({ commit }, uri) {
      return new Promise( (resolve, reject) => {
        commit("upload_request");
        axios({
          url: `${API_BASE}uris`,
          data: uri,
          method: "POST"
        })
          .then( resp => {
            console.log(resp);
            commit("upload_success");
            resolve(resp);
          })
          .catch( err => {
            commit("upload_error");
            reject(err);
          })
      })
    },
    deleteUri({ commit }, shortUri) {
      return new Promise( (resolve, reject) => {
        commit("delete_request");
        axios({
          url: `${API_BASE}uris/${shortUri}`,
          method: "DELETE"
        })
          .then( resp => {
            console.log(resp);
            commit("delete_success");
            resolve(resp);
          })
          .catch( err => {
            commit("delete_error");
            reject(err);
          })
      })
    },
    activateUri({ commit }, shortUri) {
      return new Promise( (resolve, reject) => {
        commit("activate_request");
        axios({
          url: `${API_BASE}uris/${shortUri}/activate`,
          method: "PUT"
        })
          .then( resp => {
            console.log(resp);
            commit("activate_success");
            resolve(resp);
          })
          .catch( err => {
            commit("activate_error");
            reject(err);
          })
      })
    },
    deactivateUri({ commit }, shortUri) {
      return new Promise( (resolve, reject) => {
        commit("deactivate_request");
        axios({
          url: `${API_BASE}uris/${shortUri}/deactivate`,
          method: "PUT"
        })
          .then( resp => {
            console.log(resp);
            commit("deactivate_success");
            resolve(resp);
          })
          .catch( err => {
            commit("deactivate_error");
            reject(err);
          })
      })
    },
    addHook({ commit }, hook) {
      return new Promise( (resolve, reject) => {
        commit("upload_request");
        axios({
          url: `${API_BASE}hooks`,
          data: hook,
          method: "POST"
        })
          .then( resp => {
            console.log(resp);
            commit("upload_success");
            resolve(resp);
          })
          .catch( err => {
            commit("upload_error");
            console.log(hook);
            reject(err);
          })
      })
    },
    deleteHook({ commit }, hookId) {
      return new Promise( (resolve, reject) => {
        commit("delete_request");
        axios({
          url: `${API_BASE}hooks/${hookId}`,
          method: "DELETE"
        })
          .then( resp => {
            console.log(resp);
            commit("delete_success");
            resolve(resp);
          })
          .catch( err => {
            commit("delete_error");
            reject(err);
          })
      })
    },
    activateHook({ commit }, hookId) {
      return new Promise( (resolve, reject) => {
        commit("activate_request");
        axios({
          url: `${API_BASE}hooks/${hookId}/activate`,
          method: "PUT"
        })
          .then( resp => {
            console.log(resp);
            commit("activate_success");
            resolve(resp);
          })
          .catch( err => {
            commit("activate_error");
            reject(err);
          })
      })
    },
    deactivateHook({ commit }, hookId) {
      return new Promise( (resolve, reject) => {
        commit("deactivate_request");
        axios({
          url: `${API_BASE}hooks/${hookId}/deactivate`,
          method: "PUT"
        })
          .then( resp => {
            console.log(resp);
            commit("deactivate_success");
            resolve(resp);
          })
          .catch( err => {
            commit("deactivate_error");
            reject(err);
          })
      })
    },
    updateHook({ commit }, hook) {
      return new Promise( (resolve, reject) => {
        commit("deactivate_request");
        axios({
          url: `${API_BASE}uris/${hook.hookId}`,
          data: hook,
          method: "PUT"
        })
          .then( resp => {
            console.log(resp);
            commit("deactivate_success");
            resolve(resp);
          })
          .catch( err => {
            commit("deactivate_error");
            reject(err);
          })
      })
    },
    retrieveHooks({ commit }, uriId) {
      return new Promise( (resolve, reject) => {
        commit("retrieve_request");
        axios({
          url: `${API_BASE}uris/${uriId}/hooks`,
          method: "GET"
        })
          .then( resp => {
            console.log(resp);
            localStorage.setItem("hooks",resp.data);
            commit("retrieveHook_success", resp.data);
            resolve(resp);
          })
          .catch( err => {
            console.log(err);
            commit("retrieve_error");
            localStorage.removeItem("hooks");
            reject(err);
          })
      });
    },
    getData({ commit }, type) {
      return new Promise( (resolve, reject) => {
        commit("retrieve_request");
        axios({
          url: `${DATA_API_BASE}?query=%7B%0A%20%20user%3A%20rawAccess(user%3A${localStorage.getItem("public_id")})%7B%0A%20%20%20%20datetime%0A%20%20%20%20ip%0A%20%20%20%20uri%0A%20%20%20%20datetime%0A%20%20%20%20ip%0A%20%20%20%20os%0A%20%20%7D%0A%7D`,
          headers: {Accept: type},
          method: "GET"
        })
          .then( resp => {
            console.log(resp);
            commit("retrieveData_success");

            if (type === "application/json") {
              var fileURL = window.URL.createObjectURL(new Blob([JSON.stringify(resp.data)], { type: "text/plain;charset=utf-8" }));
              var fileLink = document.createElement('a');

              fileLink.href = fileURL;
              fileLink.setAttribute('download', 'data.json');
            } else if (type === "text/csv") {
              var fileURL = window.URL.createObjectURL(new Blob([resp.data]));
              var fileLink = document.createElement('a');

              fileLink.href = fileURL;
              fileLink.setAttribute('download', 'data.csv');
            } else if (type === "application/pdf") {
              var fileURL = window.URL.createObjectURL(new Blob([resp.data]));
              var fileLink = document.createElement('a');

              fileLink.href = fileURL;
              fileLink.setAttribute('download', 'data.pdf');
            } else {
              console.log("Not a valid type of data.")
            }

            document.body.appendChild(fileLink);

            fileLink.click();
            resolve(resp);
          })
          .catch( err => {
            console.log(err);
            commit("retrieve_error");
            reject(err);
          })
      });
    },
  },
  getters: {
    isLoggedIn: state => !!state.public_id,
    token: state => state.token,
    authStatus: state => state.status,
    user: state => state.public_id,
    name: state => state.name,
    uris: state => state.uris,
    hooks: state => state.hooks
  }
});
