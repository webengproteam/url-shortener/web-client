/*
 * URL Shortener API
 * This API is responsible of authetication and the creation, modification and deletion of uris and hooks.
 *
 * OpenAPI spec version: 0.2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.9
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.UrlShortenerApi);
  }
}(this, function(expect, UrlShortenerApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new UrlShortenerApi.UserApi();
  });

  describe('(package)', function() {
    describe('UserApi', function() {
      describe('createUser', function() {
        it('should call createUser successfully', function(done) {
          // TODO: uncomment, update parameter values for createUser call and complete the assertions
          /*
          var body = new UrlShortenerApi.UserRequest();
          body.email = "";
          body.pass = "";

          instance.createUser(body, function(error, data, response) {
            if (error) {
              done(error);
              return;
            }
            // TODO: update response assertions
            expect(data).to.be.a(UrlShortenerApi.User);
            expect(data.userId).to.be.a('number');
            expect(data.userId).to.be("0");
            expect(data.email).to.be.a('string');
            expect(data.email).to.be("");

            done();
          });
          */
          // TODO: uncomment and complete method invocation above, then delete this line and the next:
          done();
        });
      });
      describe('deleteUser', function() {
        it('should call deleteUser successfully', function(done) {
          // TODO: uncomment, update parameter values for deleteUser call
          /*
          var userId = 789;

          instance.deleteUser(userId, function(error, data, response) {
            if (error) {
              done(error);
              return;
            }

            done();
          });
          */
          // TODO: uncomment and complete method invocation above, then delete this line and the next:
          done();
        });
      });
      describe('getURIByUser', function() {
        it('should call getURIByUser successfully', function(done) {
          // TODO: uncomment, update parameter values for getURIByUser call and complete the assertions
          /*
          var userId = 789;

          instance.getURIByUser(userId, function(error, data, response) {
            if (error) {
              done(error);
              return;
            }
            // TODO: update response assertions
            let dataCtr = data;
            expect(dataCtr).to.be.an(Array);
            expect(dataCtr).to.not.be.empty();
            for (let p in dataCtr) {
              let data = dataCtr[p];
              expect(data).to.be.a(UrlShortenerApi.URI);
              expect(data.uriId).to.be.a('string');
              expect(data.uriId).to.be("");
              expect(data.userId).to.be.a('number');
              expect(data.userId).to.be("0");
              expect(data.realURI).to.be.a('string');
              expect(data.realURI).to.be("");
              expect(data.shortURI).to.be.a('string');
              expect(data.shortURI).to.be("");
              expect(data.activated).to.be.a('boolean');
              expect(data.activated).to.be(false);
              expect(data.reachable).to.be.a('boolean');
              expect(data.reachable).to.be(false);
              expect(data.safe).to.be.a('boolean');
              expect(data.safe).to.be(false);
            }

            done();
          });
          */
          // TODO: uncomment and complete method invocation above, then delete this line and the next:
          done();
        });
      });
      describe('getUserById', function() {
        it('should call getUserById successfully', function(done) {
          // TODO: uncomment, update parameter values for getUserById call and complete the assertions
          /*
          var userId = 789;

          instance.getUserById(userId, function(error, data, response) {
            if (error) {
              done(error);
              return;
            }
            // TODO: update response assertions
            expect(data).to.be.a(UrlShortenerApi.User);
            expect(data.userId).to.be.a('number');
            expect(data.userId).to.be("0");
            expect(data.email).to.be.a('string');
            expect(data.email).to.be("");

            done();
          });
          */
          // TODO: uncomment and complete method invocation above, then delete this line and the next:
          done();
        });
      });
      describe('loginUser', function() {
        it('should call loginUser successfully', function(done) {
          // TODO: uncomment, update parameter values for loginUser call and complete the assertions
          /*
          var email = "email_example";
          var password = "password_example";

          instance.loginUser(email, password, function(error, data, response) {
            if (error) {
              done(error);
              return;
            }
            // TODO: update response assertions
            expect(data).to.be.a('string');
            // expect(data).to.be(null);

            done();
          });
          */
          // TODO: uncomment and complete method invocation above, then delete this line and the next:
          done();
        });
      });
      describe('logoutUser', function() {
        it('should call logoutUser successfully', function(done) {
          // TODO: uncomment logoutUser call
          /*

          instance.logoutUser(function(error, data, response) {
            if (error) {
              done(error);
              return;
            }

            done();
          });
          */
          // TODO: uncomment and complete method invocation above, then delete this line and the next:
          done();
        });
      });
    });
  });

}));
