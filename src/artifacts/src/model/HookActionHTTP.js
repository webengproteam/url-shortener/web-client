/*
 * URL Shortener API
 * This API is responsible of authetication and the creation, modification and deletion of uris and hooks.
 *
 * OpenAPI spec version: 0.2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.9
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.UrlShortenerApi) {
      root.UrlShortenerApi = {};
    }
    root.UrlShortenerApi.HookActionHTTP = factory(root.UrlShortenerApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The HookActionHTTP model module.
   * @module model/HookActionHTTP
   * @version 0.2.0
   */

  /**
   * Constructs a new <code>HookActionHTTP</code>.
   * @alias module:model/HookActionHTTP
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>HookActionHTTP</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/HookActionHTTP} obj Optional instance to populate.
   * @return {module:model/HookActionHTTP} The populated <code>HookActionHTTP</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('endpoint'))
        obj.endpoint = ApiClient.convertToType(data['endpoint'], 'String');
      if (data.hasOwnProperty('verb'))
        obj.verb = ApiClient.convertToType(data['verb'], 'String');
      if (data.hasOwnProperty('params'))
        obj.params = ApiClient.convertToType(data['params'], 'String');
      if (data.hasOwnProperty('body'))
        obj.body = ApiClient.convertToType(data['body'], Object);
    }
    return obj;
  }

  /**
   * @member {String} endpoint
   */
  exports.prototype.endpoint = undefined;

  /**
   * @member {String} verb
   */
  exports.prototype.verb = undefined;

  /**
   * @member {String} params
   */
  exports.prototype.params = undefined;

  /**
   * @member {Object} body
   */
  exports.prototype.body = undefined;

  return exports;

}));
