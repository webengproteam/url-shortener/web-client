# UrlShortenerApi.HookApi

All URIs are relative to *https://localhost/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activateHook**](HookApi.md#activateHook) | **PUT** /hooks/{hookId}/activate | Activate a hook
[**createHook**](HookApi.md#createHook) | **POST** /hooks | Create hook
[**deactivateHook**](HookApi.md#deactivateHook) | **PUT** /hooks/{hookId}/deactivate | Deactivate a hook
[**deleteHook**](HookApi.md#deleteHook) | **DELETE** /hooks/{hookId} | Deletes a hook
[**getHook**](HookApi.md#getHook) | **GET** /hooks/{hookId} | Retrieve a hook
[**getHooksByURI**](HookApi.md#getHooksByURI) | **GET** /uris/{uriId}/hooks | Retrieves all the hooks of a URI
[**updateHook**](HookApi.md#updateHook) | **PUT** /hooks/{hookId} | Update hook


<a name="activateHook"></a>
# **activateHook**
> Hook activateHook(hookId)

Activate a hook



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.HookApi();

var hookId = 789; // Number | ID of hook to activate


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.activateHook(hookId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hookId** | **Number**| ID of hook to activate | 

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createHook"></a>
# **createHook**
> Hook createHook(body)

Create hook

### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.HookApi();

var body = new UrlShortenerApi.HookRequest(); // HookRequest | Created hook object


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createHook(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**HookRequest**](HookRequest.md)| Created hook object | 

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deactivateHook"></a>
# **deactivateHook**
> Hook deactivateHook(hookId)

Deactivate a hook



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.HookApi();

var hookId = 789; // Number | ID of hook to deactivate


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.deactivateHook(hookId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hookId** | **Number**| ID of hook to deactivate | 

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteHook"></a>
# **deleteHook**
> deleteHook(hookId)

Deletes a hook



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.HookApi();

var hookId = 789; // Number | Hook id to delete


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.deleteHook(hookId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hookId** | **Number**| Hook id to delete | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getHook"></a>
# **getHook**
> Hook getHook(hookId)

Retrieve a hook



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.HookApi();

var hookId = 789; // Number | ID of Hook to retrieve


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getHook(hookId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hookId** | **Number**| ID of Hook to retrieve | 

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getHooksByURI"></a>
# **getHooksByURI**
> [Hook] getHooksByURI(uriId)

Retrieves all the hooks of a URI

### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.HookApi();

var uriId = "uriId_example"; // String | ID of URI to get hooks from


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getHooksByURI(uriId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uriId** | **String**| ID of URI to get hooks from | 

### Return type

[**[Hook]**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateHook"></a>
# **updateHook**
> Hook updateHook(hookId, body)

Update hook

### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.HookApi();

var hookId = 789; // Number | ID of Hook to retrieve

var body = new UrlShortenerApi.HookRequest(); // HookRequest | Created hook object


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateHook(hookId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hookId** | **Number**| ID of Hook to retrieve | 
 **body** | [**HookRequest**](HookRequest.md)| Created hook object | 

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

