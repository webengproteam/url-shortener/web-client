# UrlShortenerApi.URIRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**realURI** | **String** |  | [optional] 


