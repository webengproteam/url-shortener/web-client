# UrlShortenerApi.User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **Number** |  | [optional] 
**email** | **String** |  | [optional] 


