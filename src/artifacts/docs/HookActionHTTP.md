# UrlShortenerApi.HookActionHTTP

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**endpoint** | **String** |  | [optional] 
**verb** | **String** |  | [optional] 
**params** | **String** |  | [optional] 
**body** | **Object** |  | [optional] 


