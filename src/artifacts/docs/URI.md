# UrlShortenerApi.URI

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uriId** | **String** |  | [optional] 
**userId** | **Number** |  | [optional] 
**realURI** | **String** |  | [optional] 
**shortURI** | **String** |  | [optional] 
**activated** | **Boolean** |  | [optional] 
**reachable** | **Boolean** |  | [optional] 
**safe** | **Boolean** |  | [optional] 


