# UrlShortenerApi.HookRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uriId** | **String** |  | [optional] 
**trigger** | [**HookTrigger**](HookTrigger.md) |  | [optional] 
**actionsHTTP** | [**[HookActionHTTP]**](HookActionHTTP.md) |  | [optional] 
**actionsMail** | [**[HookActionMail]**](HookActionMail.md) |  | [optional] 


