# UrlShortenerApi.HookActionMail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**destinations** | **[String]** |  | [optional] 
**subject** | **String** |  | [optional] 
**text** | **String** |  | [optional] 


