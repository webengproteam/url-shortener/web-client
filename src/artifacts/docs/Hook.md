# UrlShortenerApi.Hook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hookId** | **Number** |  | [optional] 
**userId** | **Number** |  | [optional] 
**uriId** | **String** |  | [optional] 
**active** | **Boolean** |  | [optional] 
**trigger** | [**HookTrigger**](HookTrigger.md) |  | [optional] 
**actionsHTTP** | [**[HookActionHTTP]**](HookActionHTTP.md) |  | [optional] 
**actionsMail** | [**[HookActionMail]**](HookActionMail.md) |  | [optional] 


