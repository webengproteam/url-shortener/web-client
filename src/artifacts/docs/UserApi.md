# UrlShortenerApi.UserApi

All URIs are relative to *https://localhost/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUser**](UserApi.md#createUser) | **POST** /users | Create user
[**deleteUser**](UserApi.md#deleteUser) | **DELETE** /users/{userId} | Delete user
[**getURIByUser**](UserApi.md#getURIByUser) | **GET** /users/{userId}/uris | Retrieves all the URI of a user
[**getUserById**](UserApi.md#getUserById) | **GET** /users/{userId} | Get user by user id
[**loginUser**](UserApi.md#loginUser) | **GET** /users/login | Logs user into the system
[**logoutUser**](UserApi.md#logoutUser) | **GET** /users/logout | Logs out current logged in user session


<a name="createUser"></a>
# **createUser**
> User createUser(body)

Create user

This can only be done by the logged in user.

### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UserApi();

var body = new UrlShortenerApi.UserRequest(); // UserRequest | Created user object


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createUser(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserRequest**](UserRequest.md)| Created user object | 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteUser"></a>
# **deleteUser**
> deleteUser(userId)

Delete user

This can only be done by the logged in user.

### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UserApi();

var userId = 789; // Number | The id that needs to be deleted


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.deleteUser(userId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **Number**| The id that needs to be deleted | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getURIByUser"></a>
# **getURIByUser**
> [URI] getURIByUser(userId)

Retrieves all the URI of a user

### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UserApi();

var userId = 789; // Number | ID of user to get URI from


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getURIByUser(userId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **Number**| ID of user to get URI from | 

### Return type

[**[URI]**](URI.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserById"></a>
# **getUserById**
> User getUserById(userId)

Get user by user id



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UserApi();

var userId = 789; // Number | The id that needs to be fetched.


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getUserById(userId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **Number**| The id that needs to be fetched. | 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="loginUser"></a>
# **loginUser**
> 'String' loginUser(email, password)

Logs user into the system



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UserApi();

var email = "email_example"; // String | The user email for login

var password = "password_example"; // String | The password for login in clear text


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.loginUser(email, password, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| The user email for login | 
 **password** | **String**| The password for login in clear text | 

### Return type

**'String'**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="logoutUser"></a>
# **logoutUser**
> logoutUser()

Logs out current logged in user session



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UserApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.logoutUser(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

