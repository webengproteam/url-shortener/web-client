# UrlShortenerApi.HookTrigger

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clicks** | **Number** |  | [optional] 
**minutes** | **Number** |  | [optional] 


