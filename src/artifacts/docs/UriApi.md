# UrlShortenerApi.UriApi

All URIs are relative to *https://localhost/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activateURI**](UriApi.md#activateURI) | **PUT** /uris/{uriId}/activate | Activate a URI
[**addURI**](UriApi.md#addURI) | **POST** /uris | Add a new URI
[**deactivateURI**](UriApi.md#deactivateURI) | **PUT** /uris/{uriId}/deactivate | Deactivate a URI
[**deleteURI**](UriApi.md#deleteURI) | **DELETE** /uris/{uriId} | Deletes a URI
[**getHooksByURI**](UriApi.md#getHooksByURI) | **GET** /uris/{uriId}/hooks | Retrieves all the hooks of a URI
[**getURI**](UriApi.md#getURI) | **GET** /uris/{uriId} | Find URI by ID
[**getURIByUser**](UriApi.md#getURIByUser) | **GET** /users/{userId}/uris | Retrieves all the URI of a user


<a name="activateURI"></a>
# **activateURI**
> URI activateURI(uriId)

Activate a URI



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UriApi();

var uriId = "uriId_example"; // String | ID of URI to activate


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.activateURI(uriId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uriId** | **String**| ID of URI to activate | 

### Return type

[**URI**](URI.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="addURI"></a>
# **addURI**
> URI addURI(body)

Add a new URI



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UriApi();

var body = new UrlShortenerApi.URIRequest(); // URIRequest | URI that needs to be created


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.addURI(body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**URIRequest**](URIRequest.md)| URI that needs to be created | 

### Return type

[**URI**](URI.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deactivateURI"></a>
# **deactivateURI**
> URI deactivateURI(uriId)

Deactivate a URI



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UriApi();

var uriId = "uriId_example"; // String | ID of URI to deactivate


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.deactivateURI(uriId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uriId** | **String**| ID of URI to deactivate | 

### Return type

[**URI**](URI.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteURI"></a>
# **deleteURI**
> deleteURI(uriId)

Deletes a URI



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UriApi();

var uriId = "uriId_example"; // String | URI id to delete


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.deleteURI(uriId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uriId** | **String**| URI id to delete | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getHooksByURI"></a>
# **getHooksByURI**
> [Hook] getHooksByURI(uriId)

Retrieves all the hooks of a URI

### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UriApi();

var uriId = "uriId_example"; // String | ID of URI to get hooks from


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getHooksByURI(uriId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uriId** | **String**| ID of URI to get hooks from | 

### Return type

[**[Hook]**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getURI"></a>
# **getURI**
> URI getURI(uriId)

Find URI by ID



### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UriApi();

var uriId = "uriId_example"; // String | ID of URI to return


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getURI(uriId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uriId** | **String**| ID of URI to return | 

### Return type

[**URI**](URI.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getURIByUser"></a>
# **getURIByUser**
> [URI] getURIByUser(userId)

Retrieves all the URI of a user

### Example
```javascript
var UrlShortenerApi = require('url_shortener_api');

var apiInstance = new UrlShortenerApi.UriApi();

var userId = 789; // Number | ID of user to get URI from


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getURIByUser(userId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **Number**| ID of user to get URI from | 

### Return type

[**[URI]**](URI.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

