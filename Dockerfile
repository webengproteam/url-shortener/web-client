FROM node:10.17.0

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/

RUN npm install

COPY . /usr/src/app/
RUN npm run build

###################################3
FROM node:alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN npm install -g serve

COPY --from=0 /usr/src/app/dist/ .

EXPOSE 8080

ENTRYPOINT ["serve", "-s", ".", "-p", "8080"]
