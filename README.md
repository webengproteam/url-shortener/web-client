# Web client

**Master:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/web-client/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/web-client/commits/master)
**Test:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/web-client/badges/test/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/web-client/commits/test)

Deployed in [https://webengproteam.gitlab.io/url-shortener/web-client](https://webengproteam.gitlab.io/url-shortener/web-client).


## Usage

``` bash
# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

# run linter
npm run lint

# run unit tests
npm run test:unit

# run e2e tests
npm run test:e2e

```

For a detailed explanation on how things work, check out the [Vue CLI Guide](https://cli.vuejs.org/guide/).